<?php

namespace Database\Seeders;

use App\Models\Merchant;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class MerchantsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $merchants = config('merchants');

        foreach ($merchants as $alias => $merchant) {
            Merchant::firstOrCreate([
                'alias' => $alias
            ], [
                'name' => ucfirst(Str::camel($alias)),
                'alias' => $alias,
                'secret' => Str::random(),
                'enabled' => true,
                'priority' => $merchant['priority'],
            ]);
        }
    }
}
