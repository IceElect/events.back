<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPromocodes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ticket_user', function (Blueprint $table) {
            $table->string('promocode', 100)->nullable();
        });

        Schema::table('transactions', function (Blueprint $table) {
            $table->string('promocode', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_ticket', function (Blueprint $table) {
            //
        });
    }
}
