<?php

use App\Models\Event;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('description');
            $table->string('cover')->nullable();
            $table->string('hold_map')->nullable();
            $table->string('address');
            $table->integer('places');
            $table->datetime('start_at');
            $table->datetime('end_at');
            $table->decimal('latitude', 10, 7);
            $table->decimal('longitude', 10, 7);
            // $table->foreignId('organizator_id')->constraint('users');
            $table->enum('status', collect(Event::STATUSES)->keys()->toArray());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
