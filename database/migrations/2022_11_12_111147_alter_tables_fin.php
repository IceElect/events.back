<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTablesFin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->string('fin_shop_id')->nullable();
        });

        Schema::table('tickets', function (Blueprint $table) {
            $table->string('fin_item_id')->nullable();
        });

        Schema::table('transactions', function (Blueprint $table) {
            $table->string('external_id', 64)->nullable();
            $table->jsonb('external_result')->default(json_encode(new \stdClass));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {
            //
        });
    }
}
