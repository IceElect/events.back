<?php

namespace App\Dto\Merchants;

use App\Dto\MerchantParams;

class InterkassaMerchantParams extends MerchantParams
{
    /**
     * @var string|null
     */
    public $purseId;

    /**
     * @var string
     */
    public $accountId;

    /**
     * @var string
     */
    public $checkoutId;

    /**
     * @var string
     */
    public $checkoutSecretKey;

    /**
     * @var string
     */
    public $authorizationKey;

    /**
     * @var bool
     */
    public $testMode = true;
}
