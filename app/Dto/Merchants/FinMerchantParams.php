<?php

namespace App\Dto\Merchants;

use App\Dto\MerchantParams;

class FinMerchantParams extends MerchantParams
{
    /**
     * @var string
     */
    public string $apiUrl;

    /**
     * @var string
     */
    public string $username;

    /**
     * @var string
     */
    public string $password;

    /**
     * @var string
     */
    public string $secret;
}
