<?php

namespace App\Dto;

use App\Contracts\MerchantParamsContract;
use Spatie\DataTransferObject\DataTransferObject;

class MerchantParams extends DataTransferObject implements MerchantParamsContract
{
    /**
     * @var bool
     */
    protected bool $ignoreMissing = true;
}
