<?php

namespace App\Models;

use App\Dto\Columns\TransactionResultColumn;
use App\Events\TransactionCreated;
use App\Events\TransactionCreating;
use App\Events\TransactionUpdated;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    /**
     * @var string[]
     */
    const STATUSES = [
        'created' => 'created',
        'pending' => 'pending',
        'success' => 'success',
        'failure' => 'failure',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = ['amount', 'ticket_user_id', 'merchant_id', 'promocode', 'params'];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'params' => 'object',
        'external_result' => 'object',
        'result' => TransactionResultColumn::class,
        // 'result' => 'object',
    ];

    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'creating' => TransactionCreating::class,
        'created' => TransactionCreated::class,
        'updated' => TransactionUpdated::class,
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function merchant(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Merchant::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ticket(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(TicketUser::class, "ticket_user_id", "id");
    }

    /**
     * @return Attribute
     */
    public function paymentUrl(): Attribute
    {
        return new Attribute(
           fn () => $this->result->redirectUrl ?? null,
        );
    }
}
