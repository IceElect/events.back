<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    use HasFactory;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function event(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Event::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(User::class)
            ->using(TicketUser::class)
            ->withPivot(['qr', 'hash', 'used_at', 'status'])
            ->where(function ($query) {
                $query->where('ticket_user.created_at', '>', now()->subMinutes(20))
                    ->orWhere('ticket_user.status', TicketUser::STATUSES['paid']);
            });
    }

    /**
     * @return string
     */
    public function getTitleAttribute(): string
    {
        return implode(' ', [$this->event->title, '-', $this->name]);
    }

    /**
     * @return int
     */
    public function getRemainingAttribute(): int
    {
        return $this->count - $this->users->count();
    }

    /**
     * @param User $user
     * @return TicketUser
     */
    public function buy(User $user, string $status = 'booked', ?string $promocode): TicketUser
    {
        return TicketUser::create([
            'user_id' => $user->id,
            'ticket_id' => $this->id,
            'status' => TicketUser::STATUSES[$status],
            'promocode' => $promocode,
        ]);
    }
}
