<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;

    const STATUSES = [
        'draft' => 'draft',
        'release' => 'release',
        'ended' => 'ended',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'start_at' => 'datetime',
        'end_at' => 'datetime',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function articles() : \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Article::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tickets() : \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Ticket::class)
            ->orderBy('priority', 'desc')
            ->where('active', true)
            ->where('hidden', false);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function organizers() : \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(User::class)->withPivot(['role']);
    }

    /**
     * @return int
     */
    public function getPlacesRemainingAttribute() : int
    {
        return $this->places - $this->tickets->count();
    }
}
