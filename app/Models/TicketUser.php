<?php

namespace App\Models;

use App\Mail\TicketMail;
use Davidnadejdin\LaravelRobokassa\Robokassa;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Support\Facades\Mail;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class TicketUser extends Pivot
{
    const STATUSES = [
        'booked' => 'booked',
        'paid' => 'paid',
        'used' => 'used',
        'cancelled' => 'cancelled',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = ['user_id', 'ticket_id', 'qr', 'hash', 'status', 'promocode'];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'used_at' => 'datetime',
    ];

    /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
    public function getIncrementing()
    {
        return true;
    }

    /**
     * @return void
     */
    public static function boot()
    {
        parent::boot();

        self::created(function ($model) {
            try {
                $transaction = Transaction::create([
                    'amount' => $model->ticket->price,
                    'ticket_user_id' => $model->id,
                    'promocode' => $model->promocode,
                    'merchant_id' => 4,
                    'params' => [
                        'shopId' => $model->event->fin_shop_id,
                        'itemId' => $model->ticket->fin_item_id,
                    ],
                ]);
            } catch (\Exception $e) {
                $model->delete();
                
                throw $e;
            }
        });

        self::creating(function ($model) {
            $hash = md5(implode([$model->user_id, $model->ticket_id, time()]));
            $path = storage_path("app/public/tickets/{$hash}.png");
            $qr = QrCode::size(500)->format('png')->generate($hash, $path);

            $model->hash = $hash;
            $model->qr = "tickets/{$hash}.png";
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function event(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->ticket->event();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ticket(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Ticket::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function transaction(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Transaction::class, "ticket_user_id", "id");
    }

    /**
     * @return void
     */
    public function sendMail()
    {
        Mail::to($this->user)->send(new TicketMail($this));
    }

    /**
     * @return Attribute
     */
    public function paymentUrl(): Attribute
    {
        return new Attribute(
            fn () => $this->transaction->paymentUrl,
        );
    }
}
