<?php

namespace App\Models;

use App\Contracts\MerchantContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Merchant extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = ['name', 'alias', 'secret'];

    /**
     * @return MerchantContract
     */
    public function getService(): MerchantContract
    {   
        if (
            ! $this->merchantService
            && $this->alias
            && ($className = config("merchants.{$this->alias}.service"))
            && class_exists($className)
        ) {
            $this->merchantService = app()->makeWith($className, ['alias' => $this->alias]);
        }

        return $this->merchantService;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactions(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Transaction::class);
    }
}
