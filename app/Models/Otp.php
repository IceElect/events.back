<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Hash;

class Otp extends Model
{
    use HasFactory;

    /**
     * Атрибуты, которые должны быть преобразованы в дату
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'expired_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = ['code', 'hash', 'phone', 'expired_at'];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = ['code'];
    
    /**
     * notExpired
     */
    public static function notExpired()
    {
        return self::where('expired_at', '>', now());
    }
    
    /**
     * getUser
     *
     * @return User
     */
    public function getUser() : User
    {
        $user = User::where('phone', $this->phone)->first();
        if(!$user) {
            $user = User::create([
                'phone' => $this->phone,
                'password' => Hash::make('password')
            ]);
        }

        return $user;
    }
}
