<?php

namespace App\Mail;

use App\Models\TicketUser;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TicketMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The subject of the message.
     *
     * @var string
     */
    public $subject = "Вам оформлен билет.";

    /**
     * @var TicketUser
     */
    private $ticket;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(TicketUser $ticket)
    {
        $this->ticket = $ticket;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.ticket', [
            'ticket' => $this->ticket,
            'event' => $this->ticket->ticket->event,
        ]);
    }
}
