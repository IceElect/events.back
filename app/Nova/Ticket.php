<?php

namespace App\Nova;

use App\Models\TicketUser;
use App\Nova\TicketUser as NovaTicketUser;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Http\Requests\NovaRequest;

class Ticket extends Resource
{

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'Events';

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Ticket::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'name', 'description'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),

            Text::make(__('Name'), 'name')
                ->rules(['required', 'max:255'])
                ->sortable(),

            Boolean::make(__('Active'), 'active')->default(true),
            Boolean::make(__('Hidden'), 'hidden')->default(false),

            Textarea::make(__('Description'), 'description')
                ->rules(['max:800'])
                ->sortable(),

            BelongsTo::make(__('Event'), 'event', Event::class),

            Number::make(__('Count'), 'count')
                ->showOnIndex(fn () => !isset($this->pivot))
                ->rules(['required'])
                ->sortable(),

            Number::make(__('Price'), 'price')
                ->rules(['required'])
                ->sortable(),

            Number::make(__('Priority'), 'priority')
                ->showOnIndex(fn () => !isset($this->pivot))
                ->step(0.1)
                ->default(1)
                ->rules(['required'])
                ->sortable(),

            Text::make(__('Fin Item ID'), 'fin_item_id')
                ->nullable(),

            BelongsToMany::make(__('Owners'), 'users', User::class)->referToPivotAs(NovaTicketUser::class)
                ->fields(function () {
                    return [
                        Select::make(__('Status'), 'status')
                            ->default('booked')
                            ->options(TicketUser::STATUSES),

                        Text::make(__('Hash'), 'hash')->readonly(),
                        Image::make(__('QR'), 'qr')->readonly(),

                        DateTime::make(__('Used At'), 'used_at')
                            ->nullable(),
                    ];
                }),

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
