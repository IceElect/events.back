<?php

namespace App\Nova;

use App\Models\TicketUser;
use App\Nova\TicketUser as NovaTicketUser;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Avatar;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\MorphToMany;
use Laravel\Nova\Fields\Password;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;

class User extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\User::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'first_name', 'last_name', 'phone', 'email', 'username'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            Avatar::make('Аватар', 'avatar')->maxWidth(50),

            Text::make(__('Phone'), 'phone')
                ->sortable()
                ->rules('required', 'max:255'),

            // Text::make(__('Username'), 'username')
            //     ->sortable()
            //     ->rules('max:255'),

            Text::make(__('Name'), 'name')
                ->hideWhenCreating()
                ->hideWhenUpdating(),

            Text::make(__('First Name'), 'first_name')
                ->onlyOnForms()
                ->sortable()
                ->rules('required', 'max:255'),

            Text::make(__('Last Name'), 'last_name')
                ->onlyOnForms()
                ->sortable()
                ->rules('required', 'max:255'),

            Text::make('Email')
                ->sortable()
                ->nullable(),
                // ->rules('email', 'max:254')
                // ->creationRules('unique:users,email')
                // ->updateRules('unique:users,email,{{resourceId}}'),

            Password::make('Password')
                ->onlyOnForms()
                ->creationRules('required', 'string', 'min:8')
                ->updateRules('nullable', 'string', 'min:8'),
            
            BelongsToMany::make(__('Билеты'), 'tickets', Ticket::class)->referToPivotAs(NovaTicketUser::class)
                ->fields(function () {
                    return [
                        Select::make(__('Status'), 'status')
                            ->default('booked')
                            ->options(TicketUser::STATUSES),

                        Text::make(__('Hash'), 'hash')->readonly(),
                        Image::make(__('QR'), 'qr')->readonly(),

                        DateTime::make(__('Used At'), 'used_at')
                            ->nullable(),
                    ];
                }),

            BelongsToMany::make(__('Организуемые события'), 'events', Event::class)
                ->fields(function () {
                    return [
                        Text::make(__('Role'), 'role')->rules(['required'])
                    ];
                }),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
