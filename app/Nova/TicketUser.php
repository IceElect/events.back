<?php

namespace App\Nova;

use App\Models\TicketUser as ModelsTicketUser;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Panel;

class TicketUser extends Resource
{

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'Events';

    /**
     * Indicates if the resource should be displayed in the sidebar.
     *
     * @var bool
     */
    public static $displayInNavigation = true;

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\TicketUser::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'ticket.name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),

            BelongsTo::make(__('User'), 'user', User::class),
            BelongsTo::make(__('Ticket'), 'ticket', Ticket::class),

            Select::make(__('Status'), 'status')
                ->default('booked')
                ->options(ModelsTicketUser::STATUSES),

            Boolean::make(__('Merch Status'), 'merch_status')
                ->default(false),

            DateTime::make(__('Used At'), 'used_at')
                ->nullable(),

            DateTime::make(__('Created At'), 'created_at')
                ->readonly(),

            HasOne::make(__('Transaction'), 'transaction', Transaction::class),

            Text::make(__('Payment URL'), 'payment_url')->hideWhenCreating()->hideFromIndex()->readonly(),

            Text::make(__('Hash'), 'hash')->hideWhenCreating()->readonly(),
            Image::make(__('QR'), 'qr')->hideWhenCreating()->readonly(),

            new Panel(__('Additional'), [
                Textarea::make(__('Comment'), 'comment')->nullable(),
            ])
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
