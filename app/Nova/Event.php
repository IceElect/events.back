<?php

namespace App\Nova;

use Advoor\NovaEditorJs\NovaEditorJs;
use App\Models\Event as ModelsEvent;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Markdown;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Place;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Panel;
use OptimistDigital\MultiselectField\Multiselect;
use Pavinthan\NovaMap\Map;

class Event extends Resource
{

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'Events';

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Event::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),

            Text::make(__('Title'), 'title')
                ->rules(['required', 'max:255'])
                ->sortable(),

            Select::make(__('Status'), 'status')
                ->options(ModelsEvent::STATUSES)
                ->rules(['required'])
                ->default('draft'),

            Image::make(__('Обложка'), 'cover')
                ->nullable(),

            Image::make(__('Карта зала'), 'hold_map')
                ->hideFromIndex()
                ->nullable(),

            Markdown::make(__('Description'), 'description')
                ->rules(['required'])
                ->sortable(),

            Number::make(__('Places count'), 'places')
                ->rules(['required'])
                ->sortable(),
            
            Panel::make(__('Даты'), $this->datesFields()),
            Panel::make(__('Место'), $this->placeFields()),

            Text::make(__('Fin Shop ID'), 'fin_shop_id')
                ->nullable(),

            BelongsToMany::make(__('Организаторы'), 'organizers', User::class)
                    ->fields(function() {
                        return [
                            Text::make(__('Role'), 'role')->rules(['required'])
                        ];
                    }),

            HasMany::make(__('Новости события'), 'articles', Article::class),
            HasMany::make(__('Билеты события'), 'tickets', Ticket::class),

            // MapMarker::make('Location')
            //     ->searchProvider('google')
            //     ->searchProviderKey('AIzaSyASXS8wk9r5Fkg7ap5u1w0AmSsqRSYuCyY')
        ];
    }

    public function datesFields() {
        return [
            DateTime::make(__('Дата начала'), 'start_at')
                ->rules(['required'])
                ->sortable(),
            
            DateTime::make(__('Дата окончания'), 'end_at')
                ->rules(['required'])
                ->hideFromIndex()
                ->sortable(),
        ];
    }

    public function placeFields() {
        return [
            Text::make(__('Address'), 'address')
                ->rules(['required', 'max:255'])
                ->sortable(),

            Text::make('Latitude')->hideFromIndex(),
            Text::make('Longitude')->hideFromIndex(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
