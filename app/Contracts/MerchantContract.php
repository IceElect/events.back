<?php

namespace App\Contracts;

use App\Dto\Columns\TransactionResultColumn;
use App\Models\Transaction;
use Symfony\Component\HttpFoundation\Response;

interface MerchantContract
{
    /**
     * @param Transaction $transaction
     * @return Transaction
     */
    public function createPayment(Transaction $transaction): TransactionResultColumn;

    /**
     * @param string $method
     * @param array $input
     * @return Transaction
     */
    public function processPostback(string $method, array $input): Response;
}
