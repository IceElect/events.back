<?php

namespace App\Providers;

use App\Dto\Merchants\FinMerchantParams;
use App\Dto\Merchants\InterkassaMerchantParams;
use App\Services\Merchants\FinMerchant;
use App\Services\Merchants\InterkassaMerchant;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

class MerchantServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton(InterkassaMerchant::class, function ($app, $with) {
            $params = new InterkassaMerchantParams($this->_getMerchantParams($with['alias'] ?? 'interkassa'));

            return new InterkassaMerchant($params);
        });

        $this->app->singleton(FinMerchant::class, function ($app, $with) {
            $params = new FinMerchantParams($this->_getMerchantParams($with['alias'] ?? 'interkassa'));

            return new FinMerchant($params);
        });
    }

    /**
     * Transform params key to camel keys for DTO.
     *
     * @param string $alias
     * @return array
     */
    private function _getMerchantParams(string $alias): array
    {
        $params = config("merchants.$alias.params");

        return collect($params)->mapWithKeys(function ($value, $param) {
            return [Str::camel($param) => $value];
        })->toArray();
    }
}
