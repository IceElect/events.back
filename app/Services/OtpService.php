<?php

namespace App\Services;

use App\Exceptions\OtpSendException;
use App\Models\Otp;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class OtpService
{

    const WITH_SMS = true;
    const API_KEY = "MQehdZrCm2EfKfD9HjzYt805XUBdAxuI0wlH46wcoutzGw8DQPg9D6pxzdyk";

    /**
     * send
     *
     * @param  mixed $phone
     * @return Otp
     */
    public function send(string $phone): Otp
    {
        // $otp = $this->find($phone);
        $otp = $this->create($phone);

        if (static::WITH_SMS) {
            $text = "SL. Код для входа в аккаунт: {$otp->code}";
            // $text = $otp->code;
            // $response = Http::get('https://sms.ru/sms/send', [
            //     'json' => 1,
            //     'api_id' => 'B03213F2-E672-087E-1E88-92D554026872',
            //     'to' => $phone,
            //     'msg' => $text,
            // ]);
            $response = Http::post('https://admin.p1sms.ru/apiSms/create', [
                'apiKey' => static::API_KEY,
                'sms' => [
                    [
                        'text' => $text,
                        'phone' => $phone,
                        'sender' => 'VIRTA',
                        'channel' => 'char',
                        // 'channel' => 'digit',
                        'tag' => 'otp',
                    ]
                ]
            ]);
        }

        Log::info('sms:', $response);

        if (empty($response) || $response->successful() && $response->json('success', false)) {
            return $otp;
        }

        throw new OtpSendException();
    }

    /**
     * find
     *
     * @param  mixed $phone
     * @return Otp
     */
    public function find(string $phone): ?Otp
    {
        return Otp::notExpired()->where('phone', $phone)
            ->orderBy('created_at', 'desc')
            ->first();
    }

    /**
     * create
     *
     * @param  mixed $phone
     * @return Otp
     */
    public function create(string $phone): Otp
    {
        $code = rand(1000, 9999);
        $hash = md5(implode(['fuck', $code]));
        $otp = Otp::create([
            'code' => $code,
            'hash' => $hash,
            'phone' => $phone,
            'expired_at' => now()->addMinutes(1)
        ]);

        return $otp;
    }

    /**
     * verify
     *
     * @param  mixed $phone
     * @param  mixed $code
     * @return Otp
     */
    public function verify(string $phone, int $code): ?Otp
    {
        return Otp::notExpired()->where(['phone' => $phone, 'code' => $code])
            ->orderBy('created_at', 'desc')
            ->first();
    }
}
