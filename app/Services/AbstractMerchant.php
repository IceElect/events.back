<?php

namespace App\Services;

use App\Contracts\MerchantContract;
use App\Contracts\MerchantParamsContract;
use App\Dto\Columns\TransactionResultColumn;
use App\Dto\Merchants\InterkassaMerchantParams;
use App\Models\Transaction;
use Symfony\Component\HttpFoundation\Response;

abstract class AbstractMerchant implements MerchantContract
{
    /**
     * @var MerchantParamsContract
     */
    protected $params;

    /**
     * @param Transaction $transaction
     * @return Transaction
     */
    abstract public function createPayment(Transaction $transaction): TransactionResultColumn;

    /**
     * Process merchant postback.
     *
     * @param string $method
     * @param array $input
     *
     * @return Response
     */
    abstract public function processPostback(string $method, array $input): Response;

    /**
     * @var string
     */
    protected $alias = '';

    /**
     * Get payment page URL.
     *
     * @param Transaction $transaction
     * @return string
     */
    protected function getPaymentPageUrl(Transaction $transaction): string
    {
        return route('payment', ['transaction' => $transaction]);
    }

    /**
     * Get webhook page URL.
     *
     * @param  string $method
     * @return string
     */
    protected function getIteractionUrl(string $method = 'webhook'): string
    {
        $merchant = $this->getMerchant();

        return route('merchant.postback', [
            'merchant' => $merchant,
            'merchantKey' => $merchant->key,
            'method' => 'webhook',
        ]);
    }
}
