<?php

namespace App\Services\Merchants;

use App\Contracts\MerchantContract;
use App\Dto\Columns\TransactionResultColumn;
use App\Models\Transaction;
use App\Services\AbstractMerchant;
use Davidnadejdin\LaravelRobokassa\Robokassa;
use Symfony\Component\HttpFoundation\Response;

class RobokassaMerchant extends AbstractMerchant
{
    /**
     * @param Transaction $transaction
     * @return TransactionResultColumn
     */
    public function createPayment(Transaction $transaction): TransactionResultColumn
    {
        $payment = Robokassa::createPayment($this->transaction->id, $this->ticket->price, "Оплата билета");
        return new TransactionResultColumn([
            'type' => 'redirect',
            'redirectUrl' => $payment->getPaymentUrl(),
        ]);
    }

    /**
     * @param string $method
     * @param array $input
     * @return Transaction
     */
    public function processPostback(string $method, array $input): Response
    {
        return response()->json(['ok' => true]);
    }
}
