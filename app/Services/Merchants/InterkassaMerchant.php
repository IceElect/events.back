<?php

namespace App\Services\Merchants;

use App\Contracts\MerchantContract;
use App\Dto\Columns\TransactionResultColumn;
use App\Dto\Merchants\InterkassaMerchantParams;
use App\Models\Transaction;
use App\Services\AbstractMerchant;
use Davidnadejdin\LaravelRobokassa\Robokassa;
use Symfony\Component\HttpFoundation\Response;

class InterkassaMerchant extends AbstractMerchant
{
    /**
     * @var array
     */
    const INVOICE_STATUSES = [
        'success' => 'success',
        'canceled' => 'canceled',
        'refunded' => 'refunded',
    ];

    /**
     * @var string[]
     */
    const INVOICE_ACTIONS = [
        'process' => 'process',
    ];

    /**
     * @param InterkassaMerchantParams $params
     */
    public function __construct(InterkassaMerchantParams $params)
    {
        $this->params = $params;

        $configuration = new \Interkassa\Helper\Config();
        $configuration->setCheckoutSecretKey($this->params->checkoutSecretKey);
        $configuration->setAuthorizationKey($this->params->authorizationKey);
        $configuration->setAccountId($this->params->accountId);

        $this->client = new \Interkassa\Interkassa($configuration);
    }

    /**
     * @param Transaction $transaction
     * @return TransactionResultColumn
     */
    public function createPayment(Transaction $transaction): TransactionResultColumn
    {
        $returnUrl = 'https://events.slto.ru/profile';
        $invoiceRequest = new \Interkassa\Request\PostInvoiceRequest();
        $invoiceRequest
            ->setCheckoutId($this->params->checkoutId)
            ->setPaymentNumber($transaction->uuid)
            ->setAmount((float) $transaction->amount)
            ->setCurrency('RUB')
            ->setDescription('Оплата билета')
            ->setAction(self::INVOICE_ACTIONS['process'])
            // ->setPaywayVia($transaction->payload->merchant)
            ->setSuccessUrl($returnUrl)
            ->setPendingUrl($returnUrl)
            ->setFailUrl($returnUrl)
            ->setIteractionUrl(route('merchant.postback', [
                'merchant' => 'interkassa',
                'method' => 'webhook',
            ]));

        // foreach ($transaction->payload as $key => $value) {
        //     $invoiceRequest->setCustomField($key, $value);
        // }

        $result = $this->client->makeInvoicePaySystemLink($invoiceRequest);

        $code = $result->getCode();
        $status = $result->getStatus();
        $message = $result->getMessage();
        $data = $result->getData();

        $form = isset($data['internalForm']) ? $data['internalForm'] : $this->client->redirectForm($data);

        return new TransactionResultColumn([
            'success' => true,
            'data' => [
                'type' => 'redirect',
                'message' => __('Please go to the payment form'),
                'redirectUrl' => $this->getPaymentPageUrl($transaction),
                'form' => $form,
            ],
        ]);
    }

    /**
     * @param string $method
     * @param array $input
     * @return Transaction
     */
    public function processPostback(string $method, array $input): Response
    {
        return response()->json(['ok' => true]);
    }
}
