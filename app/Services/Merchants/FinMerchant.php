<?php

namespace App\Services\Merchants;

use App\Contracts\MerchantContract;
use App\Dto\Columns\TransactionResultColumn;
use App\Dto\Merchants\FinMerchantParams;
use App\Dto\Merchants\InterkassaMerchantParams;
use App\Models\Transaction;
use App\Services\AbstractMerchant;
use Davidnadejdin\LaravelRobokassa\Robokassa;
use Illuminate\Support\Facades\Http;
use Symfony\Component\HttpFoundation\Response;

class FinMerchant extends AbstractMerchant
{
    /**
     * @var array
     */
    const STATUSES = [
        'PAID' => Transaction::STATUSES['success'],
        'DONE' => Transaction::STATUSES['success'],
        'REJECTED' => Transaction::STATUSES['failure'],
    ];

    /**
     * @param FinMerchantParams $params
     */
    public function __construct(
        FinMerchantParams $params
    ) {
        $this->params = $params;
    }

    /**
     * @param Transaction $transaction
     * @return TransactionResultColumn
     */
    public function createPayment(Transaction $transaction): TransactionResultColumn
    {
        $data = (array) $transaction->params;

        if ($transaction->promocode) {
            $data['promocode'] = $transaction->promocode;
        }

        // try {
            $response = $this->request('orders', 'POST', $data);
        // } catch (\Exception $e) {
        //     throw new \Exception($e->getMessage());
        //     return new TransactionResultColumn([
        //         'success' => false,
        //         'message' => $e->getMessage(),
        //         'data' => [
        //             'type' => 'message',
        //             'message' => $e->getMessage(),
        //         ],
        //     ]);
        // }   

        $transaction->external_id = $response->id;
        $transaction->external_result = $response;

        return new TransactionResultColumn([
            'success' => true,
            'data' => [
                'type' => 'redirect',
                'message' => __('Please go to the payment form'),
            ],
            'redirectUrl' => $response->link
        ]);
    }

    /**
     * @param string $method
     * @param array $input
     * @return Transaction
     */
    public function processPostback(string $method, array $input): Response
    {
        if ($input['secret'] != $this->params->secret) {
            return response()->json(['ok' => false, 'error' => 'Unauthorized']);
        }

        $transaction = Transaction::where('external_id', $input['order_id'])->first();

        if (!$transaction instanceof Transaction) {
            return response()->json(['ok' => false, 'error' => 'Transaction not found']);
        }

        $transaction->status = self::STATUSES[$input['status']];
        $transaction->save();

        return response()->json(['ok' => true]);
    }

    /**
     * @param string $endpoint
     * @param string $method
     * @param array $data
     * 
     * @return \stdClass
     */
    public function request(string $endpoint, string $method = 'POST', array $data = []): \stdClass
    {
        $response = Http::asJson()
            ->acceptJson()
            ->baseUrl($this->params->apiUrl)
            ->withBasicAuth($this->params->username, $this->params->password)
            ->post($endpoint, $data);

        if ($response->successful()) {
            return $response->object();
        } else {
            throw new \Exception(
                $response->json('message', $response->getReasonPhrase()), 
                $response->json('errNum', $response->getStatusCode())
            );
        }
    }
}
