<?php

namespace App\Services\Merchants;

use App\Contracts\MerchantContract;
use App\Dto\Columns\TransactionResultColumn;
use App\Models\Transaction;
use App\Services\AbstractMerchant;
use Symfony\Component\HttpFoundation\Response;

class DefaultMerchant extends AbstractMerchant
{
    /**
     * @param Transaction $transaction
     * @return TransactionResultColumn
     */
    public function createPayment(Transaction $transaction): TransactionResultColumn
    {
        // return 'https://events.slto.ru/payment/' . $this->id;
        return new TransactionResultColumn([
            'type' => 'message',
            'data' => [
                'title' => '',
                'message' => 'Переведите деньги на карту #### #### #### ####',
            ]
        ]);
    }

    /**
     * @param string $method
     * @param array $input
     * @return Transaction
     */
    public function processPostback(string $method, array $input): Response
    {
        return response()->json(['ok' => true]);
    }
}
