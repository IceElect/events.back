<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TicketResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'event' => new EventResource($this->whenLoaded('event')),
            'price' => $this->price,
            'count' => $this->count,
            'description' => $this->description,
            'remaining' => $this->remaining,
            'priority' => $this->priority,
            'pivot' => $this->pivot,
            'status' => $this->whenPivotLoaded('status', function () {
                return $this->pivot->status;
            }),
            'used_at' => $this->whenPivotLoaded('used_at', function () {
                return $this->pivot->used_at;
            }),
            'qr' => $this->whenPivotLoaded('qr', function () {
                return asset('storage/' . $this->pivot->qr);
            }),
        ];
    }
}
