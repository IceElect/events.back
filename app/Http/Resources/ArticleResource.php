<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ArticleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'short' => $this->short,
            'thumbnail' => asset('storage/' . $this->thumbnail),
            'content' => $this->content,
            'event' => $this->whenLoaded('event'),
            'created_at' => $this->created_at,
        ];
    }
}
