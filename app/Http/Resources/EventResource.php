<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'status' => $this->status,
            'description' => $this->description,
            'cover' => asset('storage/' . $this->cover),
            'hold_map' => $this->hold_map ? asset('storage/' . $this->hold_map) : null,
            'location' => [
                'address' => $this->address,
                'latitude' => $this->latitude,
                'longitude' => $this->longitude,
            ],
            'tickets' => new TicketCollection($this->whenLoaded('tickets')),
            'articles' => new ArticleCollection($this->whenLoaded('articles')),
            'organizers' => new UserCollection($this->whenLoaded('organizers')),
            'places_remaining' => $this->places_remaining,
            'places' => $this->places,
            'start_at' => $this->start_at,
            'end_at' => $this->end_at,
        ];
    }
}
