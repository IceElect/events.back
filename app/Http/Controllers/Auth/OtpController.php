<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Resources\AuthResponse;
use App\Http\Resources\UserResource;
use App\Services\OtpService;
use Carbon\Carbon;
use Illuminate\Http\Request;

class OtpController extends Controller
{
    /**
     * @var OtpService
     */
    private $otpService;

    public function __construct(OtpService $otpService)
    {
        $this->otpService = $otpService;
    }

    public function send(Request $request)
    {
        $phone = $request->get('phone');
        return $this->otpService->send($phone);
    }

    public function verify(Request $request)
    {
        $code = $request->get('code');
        $phone = $request->get('phone');
        $otp = $this->otpService->verify($phone, $code);
        if (!$otp) {
            return response()->json([
                'success' => false,
                'error' => 'OTP_VERIFY_FAILED',
                'message' => 'OTP_VERIFY_FAILED',
            ], 401);
        }

        $user = $otp->getUser();
        $token = $user->createToken(config('app.name'));

        $otp->delete();

        return response()->json([
            'success' => true,
            'token_type' => 'Bearer',
            'access_token' => $token->accessToken,
            'expires_at' => Carbon::parse($token->token->expires_at)->toDateTimeString(),
            'user' => new UserResource($user)
        ]);
    }
}
