<?php

namespace App\Http\Controllers;

use App\Http\Resources\TicketResource;
use App\Http\Resources\TicketUserResource;
use App\Models\Ticket;
use Davidnadejdin\LaravelRobokassa\Robokassa;
use Illuminate\Http\Request;

class TicketController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function show(Ticket $ticket)
    {
        return response()->json(
            new TicketResource($ticket->load('event'))
        );
    }

    /**
     * buy
     *
     * @param  mixed $reqest
     * @param  mixed $ticket
     * @return void
     */
    public function buy(Request $request, Ticket $ticket)
    {
        $user = $request->user();

        try {
            $ticketUser = $ticket->buy(
                $user, 
                'booked', 
                $request->get('promocode')
            );

            return new TicketUserResource(
                $ticketUser->load(['ticket.event', 'transaction'])
            );
        } catch (\Exception $e) {
            return response()->json([
                'success' => false, 
                'errors' => [$e->getMessage()],
            ], 400);
        }
    }
}
