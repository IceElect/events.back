<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateProfileRequest;
use App\Http\Resources\TicketCollection;
use App\Http\Resources\TicketUserCollection;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function my(Request $request)
    {
        return response()->json(
            new UserResource(
                $request->user()->load(['tickets'])
            )
        );
    }

    /**
     * update
     *
     * @param  mixed $request
     * @return void
     */
    public function update(UpdateProfileRequest $request)
    {
        $request->user()->update($request->validated());
        return response()->json(
            new UserResource($request->user())
        );
    }

    /**
     * @param Request $request
     * 
     * @return [type]
     */
    public function tickets(Request $request)
    {
        return new TicketUserCollection(
            $request->user()->tickets()->with(['ticket.event'])->get(),
        );
    }
}
