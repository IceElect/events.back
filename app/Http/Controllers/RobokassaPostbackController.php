<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use Davidnadejdin\LaravelRobokassa\Robokassa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RobokassaPostbackController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'InvId' => 'required',
            'OutSum' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        }

        $id = $request->get('InvId');
        $sum = $request->get('OutSum');
        $transaction = Transaction::find($id);

        if ($sum < $transaction->amount) {
            return response()->json(['errors' => ['Bad out sum.']], 400);
        }

        $transaction->status = Transaction::STATUSES['created'];
        $transaction->save();
        $transaction->status = Transaction::STATUSES['success'];
        $transaction->save();
    }
}
