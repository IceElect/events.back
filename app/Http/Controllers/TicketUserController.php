<?php

namespace App\Http\Controllers;

use App\Http\Resources\TicketUserResource;
use App\Models\TicketUser;
use Illuminate\Http\Request;

class TicketUserController extends Controller
{

    public function __construct() {
        
    }

    /**
     * @param TicketUser $ticket
     * 
     * @return Response
     */
    public function show(TicketUser $ticket) 
    {
        return response()->json(
            new TicketUserResource($ticket->load('user', 'ticket', 'ticket.event'))
        );
    }

    /**
     * @param Request $request
     * @param TicketUser $ticket
     * 
     * @return Response
     */
    public function status(Request $request, TicketUser $ticket)
    {
        $status = $request->get('status');

        if($status) {
            $ticket->status = TicketUser::STATUSES[$status];
            $ticket->used_at = now();
            $ticket->save();
        }

        return response()->json(
            new TicketUserResource($ticket)
        );
    } 

    /**
     * @param Request $request
     * @param TicketUser $ticket
     * 
     * @return Response
     */
    public function merch_status(Request $request, TicketUser $ticket)
    {
        $merch_status = $request->get('merch_status');
        $ticket->merch_status = $merch_status;
        $ticket->save();

        return response()->json(
            new TicketUserResource($ticket)
        );
    }
}
