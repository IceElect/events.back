<?php

namespace App\Listeners;

use App\Events\TransactionCreating;
use Illuminate\Support\Str;

class AssignTransactionUuid
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TransactionCreating  $event
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @return void
     */
    public function handle(TransactionCreating $event)
    {
        $event->transaction->uuid = (string) Str::uuid();
    }
}
