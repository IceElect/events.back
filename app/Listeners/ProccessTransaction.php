<?php

namespace App\Listeners;

use App\Events\TransactionCreating;
use App\Models\Merchant;
use App\Models\Transaction;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class ProccessTransaction
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\TransactionCreating  $event
     * @return void
     */
    public function handle(TransactionCreating $event)
    {
        $transaction = $event->getTransaction();

        if ($transaction->merchant instanceof Merchant) {
            $service = $transaction->merchant->getService();
            $result = $service->createPayment($transaction);
            $event->transaction->result = $result;
            if ($result->success) {
                $transaction->status = Transaction::STATUSES['pending'];
            } else {
                $transaction->status = Transaction::STATUSES['failure'];
            }
        }
    }
}
