<?php

use App\Http\Controllers\ArticleController;
use App\Http\Controllers\Auth\OtpController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\PostbackController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\RobokassaPostbackController;
use App\Http\Controllers\TicketController;
use App\Http\Controllers\TicketUserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('auth')->group(function () {
    Route::post('otp/send', [OtpController::class, 'send']);
    Route::post('otp/verify', [OtpController::class, 'verify']);
});

Route::post('tickets/{ticket}/buy', [TicketController::class, 'buy'])->middleware('auth:api');
Route::resource('tickets', TicketController::class)->only(['show']);
Route::resource('articles', ArticleController::class);
Route::resource('events', EventController::class);
Route::resource('ticket-user', TicketUserController::class)->only(['show']);
Route::get('ticket-user/{ticket:hash}/find', [TicketUserController::class, 'show']);
Route::patch('ticket-user/{ticket}/status', [TicketUserController::class, 'status']);
Route::patch('ticket-user/{ticket}/merch_status', [TicketUserController::class, 'merch_status']);

Route::prefix('profile')->middleware('auth:api')->group(function () {
    Route::get('/', [ProfileController::class, 'my'])->middleware('auth:api');
    Route::put('/', [ProfileController::class, 'update'])->middleware('auth:api');
    Route::get('/tickets', [ProfileController::class, 'tickets'])->middleware('auth:api');
});

Route::get('robokassa/postback', RobokassaPostbackController::class);
Route::any('merchant/{merchant:alias}:ab$X21/postback/{method}', PostbackController::class)->name('merchant.postback');
