<?php

return [
    'interkassa' => [
        'priority' => 60,
        'service' => \App\Services\Merchants\InterkassaMerchant::class,
        'params' => [
            'purse_id' => (string) env('INTERKASSA_PURSE_ID', '409736665488'),
            'account_id' => (string) env('INTERKASSA_ACCOUNT_ID', '61ee5c6407a60d42267d61d2'),
            'checkout_id' => (string) env('INTERKASSA_CHECKOUT_ID', '61ee5cda92e81a4ef7493507'),
            'checkout_secret_key' => (string) env('INTERKASSA_CHECKOUT_SECRET_KEY', 'tpXdSwaIHTRkU7V1'),
            'authorization_key' => (string) env('INTERKASSA_AUTHORIZATION_KEY', 'YEkGVjfNeIAAuyWhsBsj5NPaNKbTEzuV'),
            'test_mode' => (bool) env('INTERKASSA_TEST_MODE', false),
        ],
    ],

    'robokassa' => [
        'priority' => 50,
        'service' => \App\Services\Merchants\RobokassaMerchant::class,
        'params' => [
            'login' => env('ROBOKASSA_LOGIN'),
            'password' => env('ROBOKASSA_PASSWORD'),
            'password2' => env('ROBOKASSA_PASSWORD2'),
            'test_mode' => env('ROBOKASSA_TEST_MODE', false),
        ]
    ],

    'default' => [
        'priority' => 40,
        'service' => \App\Services\Merchants\DefaultMerchant::class,
        'params' => [
            //
        ]
    ],

    'fin' => [
        'priority' => 100,
        'service' => \App\Services\Merchants\FinMerchant::class,
        'params' => [
            'api_url' => env('FIN_API_URL', 'https://pay.events.slto.ru'),
            'username' => env('FIN_USERNAME', 'events'),
            'password' => env('FIN_PASSWORD', 'YSk7uhjseQ3p'),
            'secret' => env('FIN_SECRET', 'events2022'),
        ]
    ]
];
